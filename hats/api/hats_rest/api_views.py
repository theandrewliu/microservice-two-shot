from django.http import JsonResponse
from django.shortcuts import render
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hats, LocationVO 
# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href", "section_number", "shelf_number"]


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "fabric",
        "color",
        "style_name",
        "location"
    ]

    encoders = {
        "location": LocationVODetailEncoder(),
    }

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        'name',
        'image',
        'id',
    ]

    def get_extra_data(self, o):
        return {'location': o.location.closet_name}




@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):

    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid hats id"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(['DELETE', 'GET', 'PUT'])
def api_show_hats(request, pk):
    if request.method == "GET":
        try:
            hats = Hats.objects.get(id=pk)
            return JsonResponse(
                hats,
                encoder=HatsDetailEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            hats = Hats.objects.get(id=pk)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatsDetailEncoder,
                safe=False,
            )

        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

    else:
        try:
            content = json.loads(request.body)
            hats = Hats.objects.get(id=pk)

            props = ['name', 'fabric', 'color', 'style_name', 'location',]
            for prop in props:
                if prop in content:
                    setattr(hats, prop, content[prop])
            hats.save()

            return JsonResponse(
                hats,
                encoder=HatsDetailEncoder,
                safe=False,
            )

        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


