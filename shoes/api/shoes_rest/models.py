from pyexpat import model
from django.db import models

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.closet_name

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    image_url = models.URLField(null=True, blank=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
