import React from "react";

// class ShoeList extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       shoes: []
//     }

//     this.handleDelete = this.handleDelete.bind(this);
//   }

//   async componentDidMount() {
//     const url = 'http://localhost:8080/api/shoes'
//     const response = await fetch(url);

//     if(response.ok) {
//       const data = await response.json();
//       this.setState({shoes: data.shoes})
//     }
//   }

//   async handleDelete(event) {
//     event.preventDefault();
//     const data={...this.state};
    
//     const shoeUrl = `http://localhost:8080/api/shoes/${data.shoe.id}`
//     const fetchConfig = {
//       method: "delete",
//       body: JSON.stringify(data),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     };
//     const response = await fetch(shoeUrl, fetchConfig);
//   }

//   render() {
//     return (
//       <table className="table table-striped">
//         <thead>
//           <tr>
//             <th>Manufacturer</th>
//             <th>Name</th>
//             <th>Color</th>
//             <th>Image</th>
//             <th>Closet</th>
//             <th></th>
//           </tr>
//         </thead>
//         <tbody>
//           {this.state.shoes.map(shoe => {
//             return (
//               <tr key={shoe.id}>
//                 <td>{ shoe.manufacturer }</td>
//                 <td>{ shoe.name }</td>
//                 <td>{ shoe.color }</td>
//                 <td><img src={ shoe.image_url } alt="null" width="20%" height="20%"/></td>
//                 <td>{ shoe.closet_name }</td>
//                 <td><button onClick={() => this.handleDelete(shoe.id)}>Delete</button></td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>
//     )
//   }
// }


function ShoeList(props) {
  console.log("me", props)
  const deleteItem = async (id) => {
    fetch(`http://localhost:8080/api/shoes/${id}`,{
      method: 'DELETE',
      headers: {'Content-Type': 'application/json'
    }
    }).then(() => {
      window.location.reload();
    })
  }

    return(
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Name</th>
          <th>Color</th>
          <th>Image</th>
          <th>Closet</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map(shoe => {
          console.log(shoe)
          return (
            <tr key={shoe.id}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.name }</td>
              <td>{ shoe.color }</td>
              <td><img src={ shoe.image_url } alt="null" width="20%" height="20%"/></td>
              <td>{ shoe.closet_name }</td>
              <td><button onClick={() => deleteItem(shoe.id)}>Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
    )
}

export default ShoeList