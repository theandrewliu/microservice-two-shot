import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadShoesHats() {
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');
  if (shoeResponse.ok && hatsResponse.ok) {
    const shoeData = await shoeResponse.json();
    const hatsData = await hatsResponse.json();
    root.render(
      <React.StrictMode>
        <App shoes={shoeData.shoes} hats={hatsData.hats} />
      </React.StrictMode>
    );
  } else {
    console.error(shoeResponse || hatsResponse);
  }
}
loadShoesHats();

// async function loadHats() {
//   const response = await fetch('http://localhost:8090/api/hats/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App hats={data.hats} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }
// loadHats();
