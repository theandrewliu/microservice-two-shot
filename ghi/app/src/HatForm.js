import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            fabric: '',
            color: '',
            style_name: '',
            locations: '',
            locations: []
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if(response.ok){
            const newHat = await response.json();
            
            const cleared = {
                name: '',
                fabric: '',
                color: '',
                style_name: '',
                location: '',
            };
            this.setState(cleared);
        }
    }


    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }
    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value });
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new Hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleStyleNameChange} placeholder="Style" type="text" id="stylename" name="stylename" className="form-control"></input>
                            <label htmlFor="stylename">Style</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                            <option value="">Locations</option>
                            {this.state.locations.map(location => {
                                return (
                                <option key={location.href} value={location.href}>{location.closet_name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

}

export default HatForm
